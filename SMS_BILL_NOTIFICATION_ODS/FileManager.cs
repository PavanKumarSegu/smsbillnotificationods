﻿using log4net;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class FileManager
    {
        public static string globalFilePath = string.Empty;
        private ILog log;
        //public static int summaryId;

        public FileManager()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(FileManager));
        }

        public List<string> GetAccountNoDaily()
        {
            try
            {
                var sharedFolderPath = ConfigurationManager.AppSettings["SHARE_FOLDER_PATH"];
                //var outputPath = ConfigurationManager.AppSettings["OUTPUT_FOLDER_PATH"];

                if (string.IsNullOrWhiteSpace(sharedFolderPath))
                {
                    log.Error(string.Format("(GetAccountNoDaily) SHARE_FOLDER_PATH appSetting key is missing from App.config \r\n"));
                    return null;
                }

                var dateTimeYday = DateTime.Now.AddDays(-1);
                var monthStr = dateTimeYday.ToString("MMM");
                sharedFolderPath = string.Format(sharedFolderPath, dateTimeYday.Year.ToString());
                sharedFolderPath = sharedFolderPath + monthStr + @"\";
                var zipFileName = createZipFileName(dateTimeYday);

                log.Info("Processing the file [" + zipFileName + "]");

                if (string.IsNullOrWhiteSpace(zipFileName))
                {
                    return null;
                }

                sharedFolderPath = sharedFolderPath + zipFileName;
                globalFilePath = sharedFolderPath;

                var fileList = new ReadSource().UnzipFiles(sharedFolderPath);

                if (fileList.IsNullOrEmpty())
                {
                    log.Error("(GetAccountNoDaily) Zip file is empty. ");
                    return null;
                }

                var fileName = new ReadSource().SearchFile(fileList, dateTimeYday);

                if (string.IsNullOrWhiteSpace(fileName))
                {
                    log.Error("(GetAccountNoDaily) Fail to find file based on given rule. ");
                    return null;
                }

                fileName = fileName.Replace("/", @"\");

                var accountNoList = new ReadSource().ReadFile(AppDomain.CurrentDomain.BaseDirectory + @"Unzipped Files\", fileName);

                return accountNoList;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(GetAccountNoDaily) Exception triggered : \r\n {0} ", ex.ToString()));
                return null;
            }
        }

        public List<string> GetAccountNoSingle()
        {
            try
            {
                var fullFilePath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["SINGLE_FILE_FULLPATH"];
                //var fullFilePath = ConfigurationManager.AppSettings["SINGLE_FILE_FULLPATH"];
                //var outputPath = ConfigurationManager.AppSettings["OUTPUT_FOLDER_PATH"];

                if (string.IsNullOrWhiteSpace(fullFilePath))
                {
                    return null;
                }

                var fileName = Path.GetFileNameWithoutExtension(fullFilePath);
                var fileNameDtime = fileName.Substring(5, fileName.Length - 5);
                var dateTimeFromZipFile = Convert.ToDateTime(fileNameDtime.Substring(0, 2) + "/" + fileNameDtime.Substring(2, 2) + "/" + DateTime.Now.Year.ToString());
                //var dateTimeFromZipFile = Convert.ToDateTime(fileNameDtime.Substring(0, 2) + "/" + fileNameDtime.Substring(2, 2) + "/" + "2016");

                globalFilePath = fullFilePath;
                log.Info("Processing the adhoc file [" + fullFilePath + "]");

                var fileList = new ReadSource().UnzipFiles(fullFilePath);

                if (fileList.IsNullOrEmpty())
                {
                    log.Error("(GetAccountNoSingle) Zip file is empty. ");
                    return null;
                }

                var textFileName = new ReadSource().SearchFile(fileList, dateTimeFromZipFile);

                if (string.IsNullOrWhiteSpace(textFileName))
                {
                    log.Error("(GetAccountNoSingle) Fail to find file based on given rule. ");
                    return null;
                }

                textFileName = textFileName.Replace("/", @"\");
                var accountNoList = new ReadSource().ReadFile(AppDomain.CurrentDomain.BaseDirectory + @"Unzipped Files\", textFileName);

                return accountNoList;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(GetAccountNoSingle) Exception triggered : \r\n {0} ", ex.ToString()));
                return null;
            }
        }

        public List<String> ReadAccNoAdhoc(string fname)
        {
            var accountNoList = new ReadSource().ReadFileAdhoc(AppDomain.CurrentDomain.BaseDirectory + @"Unzipped Files\", fname);

            return accountNoList;
        }

        public string createZipFileName(DateTime dateTime)
        {
            try
            {
                var strA = "Cycle";

                var dayOTM = dateTime.Day.ToString("D2");
                var month = dateTime.Month.ToString("D2");
                var strB = dayOTM + month;

                var strC = ".zip";

                var fileName = strA + strB + strC;

                return fileName;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(createZipFileName) Exception triggered : \r\n {0}", ex.ToString()));
                return string.Empty;
            }
        }

        public List<string> FilterAccountId(List<string> accountNoList, BillRunSummary summary, List<AccountNoLog> accNoLogList)
        {
            try
            {
                var exclusionList = new ReadExcel().Read();
                var accountNoListInt = accountNoList.Select(int.Parse).ToList();
                accountNoList = accountNoListInt.ConvertAll<string>(delegate(int i) { return i.ToString(); });
                var result = new List<string>();
                if (!exclusionList.IsNullOrEmpty())
                {
                    result = accountNoList.Except(exclusionList).ToList();
                    // todo log excluded list in db with new flag
                }

                var missingAccounts = new List<string>();

                missingAccounts = accountNoList.Intersect(exclusionList).ToList();


                foreach (var str in missingAccounts)
                {
                    var accLog = new AccountNoLog()
                    {
                        AccountNo = str,
                        SMSMessage = string.Empty,
                        bill_date = string.Empty,
                        mobile_no = string.Empty,
                        status = string.Empty,
                        fbilldate = string.Empty,
                        newcharges = 0,
                        overdueamt = 0,
                        total_amt_due = 0,
                        due_date = (DateTime)SqlDateTime.MinValue,
                        lastpaymentamount = 0,
                        lastpaymentdate = String.Empty,
                        Message = "Account No found in Excel filter list",
                        Flag = "4",
                        Origin = FileManager.globalFilePath
                    };

                    accNoLogList.Add(accLog);
                }

                summary.filteredCount += missingAccounts.Count;
                return result;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(FilterAccountId) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }
        }


        public List<string> FilterAccountSuppressionList(List<string> accountNoList, BillRunSummary summary, List<AccountNoLog> accNoLogList)
        {
            try
            {
                DateTime dt = DateTime.Now.AddDays(-2);
                string strDate=summary.cycle.Substring(5,2).ToString();
                string EndDate = dt.Year.ToString() + "-" + dt.Month.ToString() + "-" + strDate + " 00:00:00.00";
                List<string> exclusionList=new List<string>();
                using (var db = new Database("LocalDB"))
                {
                    var sqlstrFetchBatch = "select ACCOUNT_NUMBER from BillSuppressionList where  [End Date] >= '" + EndDate + "' and BILLING_CYCLE='"+Convert.ToInt32(strDate)+"'";

                    var sqlQueryBatchID = PetaPoco.Sql.Builder.Append(sqlstrFetchBatch);
                    db.CommandTimeout = 4 * 60;

                    var result1 = db.Fetch<string>(sqlQueryBatchID);
                    exclusionList=result1.ToList();
                }

                //var exclusionList = new ReadExcel().Read();
                var accountNoListInt = accountNoList.Select(int.Parse).ToList();
                accountNoList = accountNoListInt.ConvertAll<string>(delegate(int i) { return i.ToString(); });
                var result = new List<string>();
                if (!exclusionList.IsNullOrEmpty())
                {
                    result = accountNoList.Except(exclusionList).ToList();
                    // todo log excluded list in db with new flag
                }

                var missingAccounts = new List<string>();

                missingAccounts = accountNoList.Intersect(exclusionList).ToList();


                foreach (var str in missingAccounts)
                {
                    var accLog = new AccountNoLog()
                    {
                        AccountNo = str,
                        SMSMessage = string.Empty,
                        bill_date = string.Empty,
                        mobile_no = string.Empty,
                        status = string.Empty,
                        fbilldate = string.Empty,
                        newcharges = 0,
                        overdueamt = 0,
                        total_amt_due = 0,
                        due_date = (DateTime)SqlDateTime.MinValue,
                        lastpaymentamount = 0,
                        lastpaymentdate = String.Empty,
                        Message = "Account No found in Excel filter list",
                        Flag = "4",
                        Origin = FileManager.globalFilePath
                    };

                    accNoLogList.Add(accLog);
                }

                summary.filteredCount += missingAccounts.Count;
                return result;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(FilterAccountId) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }
        }


    }
}
