﻿using log4net;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class SMSManager
    {
        private ILog log;

        public SMSManager()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(SMSManager));
        }

       

        public bool RunTask(List<string> AccountNoList)
        {
            try
            {
                var summaryObj = new BillRunSummary();
                summaryObj.cycle = Path.GetFileNameWithoutExtension(FileManager.globalFilePath);
                var accNoLogList = new List<AccountNoLog>();

                var filteredAccountNoList = new FileManager().FilterAccountId(AccountNoList, summaryObj, accNoLogList);
                //var filteredAccountNoList = new FileManager().FilterAccountSuppressionList(AccountNoList, summaryObj, accNoLogList);

                if (filteredAccountNoList.IsNullOrEmpty())
                {
                    return false;
                }

                var accountsInfo = GetBillDetails(filteredAccountNoList);

                if (accountsInfo.IsNullOrEmpty())
                {
                    return false;
                }
                //return false;
                var accounts = from b in accountsInfo
                               select b.accountNo;

                var AccountNoListInt = filteredAccountNoList.Select(int.Parse).ToList();
                var AccountNoLIstStr = AccountNoListInt.Select(i => i.ToString());



                var missingAccounts = AccountNoLIstStr.Except(accounts.ToList());

                foreach (var str in missingAccounts)
                {
                    var accLog = new AccountNoLog()
                    {
                        AccountNo = str,
                        SMSMessage = string.Empty,
                        bill_date = string.Empty,
                        mobile_no = string.Empty,
                        status = string.Empty,
                        fbilldate = string.Empty,
                        newcharges = 0,
                        overdueamt = 0,
                        total_amt_due = 0,
                        due_date = (DateTime)SqlDateTime.MinValue,
                        lastpaymentamount = 0,
                        lastpaymentdate = String.Empty,
                        Message = "Account No is not found in ODS",
                        Flag = "1",
                        Origin = FileManager.globalFilePath
                    };

                    accNoLogList.Add(accLog);
                }


                summaryObj.totalCount = AccountNoList.Count;
                summaryObj.errorCount = missingAccounts.Count();
                //summaryfilter

                var billList = createSMSes(accountsInfo, summaryObj, accNoLogList);

                if (billList.IsNullOrEmpty())
                {
                    return false;
                }

                var summaryId = new SQLQueries().InsertBillRunSummary(summaryObj);

                if (!accNoLogList.IsNullOrEmpty())
                {
                    foreach (var elem in accNoLogList)
                    {
                        elem.BillRunSummaryId_FK = summaryId;
                    }
                    var dTableAccLog = ToDataTable(accNoLogList);
                    var logInsertStatus = InsertBulkBillRun(dTableAccLog, "BillNotification", "AccountNoLog");
                    if (!logInsertStatus)
                    {
                        return false;
                    }
                }

                foreach (var item in billList)
                {
                    item.BillRunSummaryId_FK = summaryId;
                }

                var dTableBill = ToDataTable(billList);

                var accList = new List<AccountNoTotalCRM>();

                foreach (var item in AccountNoList)
                {
                    var acc = new AccountNoTotalCRM()
                    {
                        ACCOUNT_ID = item,
                        BillRunSummaryId_FK = summaryId
                    };

                    accList.Add(acc);
                }

                var dTableAcc = ToDataTable(accList);

                if (InsertBulkBillRun(dTableBill, "BillNotification", "daily_billrun_ODS"))
                {
                    log.Info("Insert to daily_billrun_ODS successfull. Records committed : " + billList.Count);
                   //if (InsertSMSBatchHeaderDetail(summaryObj.cycle,summaryObj.sentCount))
                    if (InsertSMSOutGoing())
                    {
                        log.Info("Insert to MESSAGE_OUTGOING successfull. Records committed : " + billList.Count);
                        if (InsertBulkBillRun(dTableAcc, "BillNotification", "AccountNoTotalCRM"))
                        {
                            log.Info("Insert to AccountNoTotalCRM successfull. Records committed : " + accList.Count);
                            return true;
                        }
                        else
                        {
                            log.Info("Insert to AccountNoTotalCRM failed. ");
                            return false;
                        }
                    }
                    else
                    {
                        log.Info("Insert to MESSAGE_OUTGOING failed. ");
                        return false;
                    }
                }
                else
                {
                    log.Info("Insert to daily_billrun_ODS failed. ");
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured while processing", ex);
                 return false;
            }
        }
        public List<BillDetails> GetBillDetails(List<string> AccountNoList)
        {
            try
            {
                var strB = new StringBuilder();
                strB.Append("( ");
                foreach (var accountNo in AccountNoList)
                {
                    strB.Append("'" + accountNo + "',");
                }

                var accountNoStr = strB.ToString();
                accountNoStr = accountNoStr.Remove(accountNoStr.Length - 1) + " ) ";


                var billList = new SQLQueries().SelectBillDetails(accountNoStr);

                return billList;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(GetBillDetails) Exception triggered : \r\n {0}", ex.Message));
                return null;
            }
        }

    

        //public List<BillDetails> GetDupBillDetails(List<string> AccountNoList)
        //{
        //    try
        //    {
        //        var strB = new StringBuilder();
        //        strB.Append("( ");
        //        foreach (var accountNo in AccountNoList)
        //        {
        //            strB.Append("'" + accountNo + "',");
        //        }

        //        var accountNoStr = strB.ToString();
        //        accountNoStr = accountNoStr.Remove(accountNoStr.Length - 1) + " ) ";


        //        var billList = new SQLQueries().SelectBlastedBillDetails(accountNoStr);

        //        return billList;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(string.Format("(GetBillDetails) Exception triggered : \r\n {0}", ex.Message));
        //        return null;
        //    }
        //}

        public List<DailyBillRun> createSMSes(List<BillDetails> accountsInfo, BillRunSummary summary, List<AccountNoLog> accNoLogList)
        {
            try
            {
                //var SMS_SCENE1_WITH_LASTPAYMENT = "Last payment for Acc. {0}: RM{1}. Current Outstanding: RM{2} due by {3}. Info valid as of {4}/Astro";
                //var SMS_SCENE1_WITHOUT_LASTPAYMENT = "Last payment for Acc. {0}: RM0.00. Current Outstanding: RM{1} due by {2}. Info valid as of {3}/Astro";
                //var SMS_SCENE2_WITH_LASTPAYMENT = "Last payment for Acc. {0}: RM{1} . Current Outstanding: RM{2} due immediately. Info valid as of {3}/Astro";
                //var SMS_SCENE2_WITHOUT_LASTPAYMENT = "Last payment for Acc. {0}: RM0.00. Current Outstanding: RM{1} due immediately. Info valid as of {2}/Astro";
                //var SMS_SCENE3_WITH_LASTPAYMENT = "Last payment for Acc. {0}: RM{1}. There is no outstanding amount due. Info valid as of {2}/Astro";
                //var SMS_SCENE3_WITHOUT_LASTPAYMENT = "Last payment for Acc. {0}: RM0.00. There is no outstanding amount due. Info valid as of {1}/Astro";

                

                string SMS_LASTEST_SCENE1 = "Last payment for Acc. {0}: RM{1} . Current Outstanding: RM{2} due immediately. Info valid as of {3}/Astro";
                string SMS_LASTEST_SCENE2 = "Last payment for Acc. {0}: RM{1}. There is no outstanding amount due. Info valid as of {2}/Astro";
                string SMS_LASTEST_SCENE3 = "Last payment for Acc. {0}: RM{1}. Current Outstanding: RM{2} due by {3}. Info valid as of {4}/Astro";

                var message = string.Empty;
                string currDate = DateTime.Today.ToString("dd-MMM-yyyy");

                var dailyRunList = new List<DailyBillRun>();
                var flag2Counter = 0;
                var flag3Counter = 0;
                foreach (var bill in accountsInfo)
                {
                    bill.mobileNo = FormatMobile(bill.mobileNo);
                    bill.accountNo = FormatAccNo(bill.accountNo);

                    string strPymt = "";
                    if (bill.lastpaymentamount > 0)
                    {
                        strPymt = bill.lastpaymentamount.ToString("#######.00");
                    }                     
                    else
                    {
                        strPymt = "0.00";
                    }
                        
                    //New revamped scenarios VJ2016Sep28
                    if (bill.total_amt_due > 0 && bill.total_amt_due <= 5) // if outstanding is >0 and <5 then notification wont send out                        
                    {
                        //
                        string payDate = "";
                        if (bill.lastpaymentdate == null)
                            payDate = String.Empty;
                        else
                            payDate = bill.lastpaymentdate.ToShortDateString();

                        var accLog = new AccountNoLog()
                        {
                            AccountNo = bill.accountNo,
                            Message = "Account total amount due less than RM 5. ",
                            SMSMessage = string.Empty,
                            bill_date = bill.statement_date.ToString(),
                            mobile_no = bill.mobileNo,
                            status = string.Empty,
                            fbilldate = bill.statement_date.ToString("yyyy-MM-dd"),
                            newcharges = bill.newcharges,
                            overdueamt = bill.overdueamt,
                            total_amt_due = bill.total_amt_due,
                            due_date = bill.due_date,
                            lastpaymentamount = bill.lastpaymentamount,
                            lastpaymentdate = payDate,
                            Flag = "3",
                            Origin = FileManager.globalFilePath
                        };

                        accNoLogList.Add(accLog);
                        flag3Counter++;
                        continue;
                        
                    }
                    else if (bill.overdueamt > 0)
                    {
                        //msg = "pay immediately";
                        message = String.Format(SMS_LASTEST_SCENE1, bill.accountNo, strPymt, bill.total_amt_due.ToString("#######.00"), currDate);
                    }
                    else if (bill.total_amt_due <= 0)
                    {
                        //msg = "bill info notification";
                        message = String.Format(SMS_LASTEST_SCENE2, bill.accountNo, strPymt, currDate);
                    }
                    else if (bill.total_amt_due > 0)
                    {
                        //msg = "pay by due date";
                        message = String.Format(SMS_LASTEST_SCENE3, bill.accountNo, strPymt, bill.total_amt_due.ToString("#######.00"), bill.due_date.ToString("dd-MMM-yyyy"), currDate);
                    }

                    

                    #region old scenarios
                    //if (bill.newcharges > 0 && bill.overdueamt == 0 && bill.total_amt_due > 0)
                    //{
                    //    if (bill.lastpaymentamount > 0)
                    //    {
                    //        message = string.Format(SMS_SCENE1_WITH_LASTPAYMENT, bill.accountNo, bill.lastpaymentamount.ToString("#######.00"), bill.total_amt_due.ToString("#######.00"), bill.due_date, currDate);
                    //    }
                    //    else
                    //    {
                    //        message = String.Format(SMS_SCENE1_WITHOUT_LASTPAYMENT, bill.accountNo, bill.total_amt_due.ToString("#######.00"), bill.due_date, currDate);
                    //    }
                    //}
                    //else if (bill.newcharges > 0 && bill.overdueamt > 0 && bill.total_amt_due > 0) // Scenario2
                    //{
                    //    if (bill.lastpaymentamount > 0) //with payment found
                    //    {
                    //        message = String.Format(SMS_SCENE2_WITH_LASTPAYMENT, bill.accountNo, bill.lastpaymentamount.ToString("#######.00"), bill.total_amt_due.ToString("#######.00"), currDate);
                    //    }
                    //    else
                    //    {
                    //        message = String.Format(SMS_SCENE2_WITHOUT_LASTPAYMENT, bill.accountNo, bill.total_amt_due.ToString("#######.00"), currDate);
                    //    }
                    //}
                    //else if (bill.newcharges >= 0 && bill.overdueamt <= 0 && bill.total_amt_due <= 0) // Scenario3
                    //{
                    //    if (bill.lastpaymentamount > 0) //with payment found
                    //    {
                    //        message = String.Format(SMS_SCENE3_WITH_LASTPAYMENT, bill.accountNo, bill.lastpaymentamount.ToString("#######.00"), currDate);
                    //    }
                    //    else
                    //    {
                    //        message = String.Format(SMS_SCENE3_WITHOUT_LASTPAYMENT, bill.accountNo, currDate);
                    //    }
                    //}
                    #endregion old scenarios

                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        var dailyObj = new DailyBillRun()
                        {
                            message = message,
                            bill_date = bill.statement_date.ToString(),
                            mobile_no = bill.mobileNo,
                            account_id = bill.accountNo,
                            status = "N",
                            fbilldate = bill.statement_date.ToString("yyyy-MM-dd")
                        };

                        dailyRunList.Add(dailyObj);
                    }
                    else
                    {
                        string payDate = "";
                        if (bill.lastpaymentdate == null)
                            payDate = String.Empty;
                        else
                            payDate = bill.lastpaymentdate.ToShortDateString();

                        var accLog = new AccountNoLog()
                        {
                            AccountNo = bill.accountNo,
                            SMSMessage = string.Empty,
                            bill_date = bill.statement_date.ToString(),
                            mobile_no = bill.mobileNo,
                            status = string.Empty,
                            fbilldate = bill.statement_date.ToString("yyyy-MM-dd"),
                            newcharges = bill.newcharges,
                            overdueamt = bill.overdueamt,
                            total_amt_due = bill.total_amt_due,
                            due_date = bill.due_date,
                            lastpaymentamount = bill.lastpaymentamount,
                            lastpaymentdate = payDate,
                            Message = "Account escapes all scenario to create SMS message",

                            Flag = "2",
                            Origin = FileManager.globalFilePath
                        };

                        accNoLogList.Add(accLog);
                        flag2Counter++;
                        log.Info(string.Format("Account escapes all scenario to create SMS message. Account No : {0}", accLog.AccountNo));
                    }
                }

                

                summary.sentCount += dailyRunList.Count;
                summary.excludedCount += flag3Counter;
                summary.errorCount += flag2Counter;

                return dailyRunList;
            }
            catch (Exception ex)
            {
                log.Error("(createSMSes) Exception triggered", ex);
                return null;
            }
        }

        public static String FormatAccNo(String acNo)
        {
            String accNo = acNo.Replace("-", "");
            if (accNo.Length == 8)
            {
                accNo = "0" + accNo;
            }
            if (accNo.Length == 9)
            {
                accNo = accNo + CheckDigit(accNo);
            }
            return accNo;
        }

        private static decimal CheckDigit(String acctNo)
        {
            int acctnumber = int.Parse(acctNo);
            int total = 0;
            for (int i = 9; i > 0; i--)
            {
                total += (acctnumber % 10) * (11 - i);
                acctnumber = Convert.ToInt32(Decimal.Truncate(acctnumber / 10));
            }
            int a = total % 11;
            if (a >= 10)
                a = 0;
            return a;
        }


        private String FormatMobile(String mobile)
        {
            try
            {
                if (mobile == null || String.IsNullOrWhiteSpace(mobile))
                    return string.Empty;

                if (!mobile.StartsWith("6"))
                    mobile = "6" + mobile;

                mobile = mobile.Replace("-", String.Empty).Replace("/", String.Empty).Replace(" ", String.Empty).Replace("\\", String.Empty).Replace("_", String.Empty);

                if (mobile.StartsWith("603"))
                    return string.Empty;
                if (mobile.Length < 11)
                    return string.Empty;
                return mobile;
            }
            catch (Exception ex)
            {
                log.Error("(FormatMobile) Exception triggered", ex);
                return string.Empty;
            }
        }


        public bool InsertBulkBillRun(DataTable dTable, string connStrParamName, string tableName)
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings[connStrParamName].ConnectionString;

                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    using (var bulkCopy = new SqlBulkCopy(conn))
                    {
                        bulkCopy.DestinationTableName = tableName;

                        foreach (var column in dTable.Columns)
                        {
                            bulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());
                        }
                        bulkCopy.BulkCopyTimeout = 3600;
                        bulkCopy.WriteToServer(dTable);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Error("(InsertBulkBillRun - tableName: "+ tableName +") Exception triggered", ex);
                return false;
            }
        }

        public bool InsertSMSOutGoing()
        {
            try
            {
                var system_id = ConfigurationManager.AppSettings["SYSTEM_ID"];

                using (var db = new Database("PartnerDB"))
                {
                    var sqlStr = "INSERT INTO [SMSGateway].[dbo].[MESSAGE_OUTGOING] ([TIMESTAMP] " +
                                    " ,[MOBILE] " +
                                    " ,[MESSAGE] " +
                                    " ,[STATUS] " +
                                    " ,[SYSTEM_ID] " +
                                    " ,[REFERENCE_DATA] " +
                                    ", [PRIORITY]) " + 
                                    " (SELECT GETDATE(), MOBILE_NO, MESSAGE, 'N', '" + system_id + "', ID, '2'  FROM [SMS_BILL_NOTIFICATION].[dbo].[daily_billrun_ODS] WHERE STATUS = 'N') " +

                                    " UPDATE [SMS_BILL_NOTIFICATION].[dbo].[daily_billrun_ODS] SET STATUS = 'C' WHERE STATUS = 'N' ";

                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr, system_id);

                    db.Execute(sqlStr);

                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("(InsertSMSOutGoing) Exception triggered", ex);
                return false;
            }
        }



        public bool InsertSMSBatchHeaderDetail(string cycleName,int sentCount)
        {
            try
            {
                var system_id = ConfigurationManager.AppSettings["SYSTEM_ID"];

                using (var db = new Database("PartnerDB"))
                {
                    var sqlStrBatch="INSERT INTO [dbo].[BATCH_HEADER]"+
                                " ([SYSTEM_ID] "+
                                   " ,[FILENAME] "+
                                   ",[TIME_START]"+
                                   ",[TIME_END]"+
                                   ",[NO_OF_RECORD]"+
                                   ",[STATUS]"+
                                   ",[ERROR_LOG]"+
                                   ",[SCHEDULE_DATE]"+
                                   ",[PRIORITY])"+
                             "VALUES"+
                                  " ('" + system_id + "'" +
                                  " ,'" + cycleName + "'" +
                                  " ,GETDATE()"+
                                   ",GETDATE()"+
                                   ",'"+sentCount+"'"+
                                   ",'C'"+
                                   ",''"+
                                   ",'"+DateTime.MinValue+"'"+
                                   ",'3')";


                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStrBatch, system_id);

                    db.Execute(sqlStrBatch);

                    var sqlstrFetchBatch = "SELECT top 1 ID  from BATCH_HEADER order by ID desc";

                    var sqlQueryBatchID = PetaPoco.Sql.Builder.Append(sqlstrFetchBatch);
                    db.CommandTimeout = 4 * 60;
                    
                    var result = db.Fetch<BatchHeaderValue>(sqlQueryBatchID);


                    var sqlStrBatchDetail = "INSERT INTO [SMSGateway].[dbo].[BATCH_DETAIL] ([BATCH_ID] " +
                                    " ,[MOBILE] " +
                                    " ,[MESSAGE] " +
                                    " ,[REFERENCE_DATA] " +
                                    " ,[STATUS] " +
                                    " ,[ERROR_LOG]) " +
                                    " (SELECT '"+result[0].ID+"', MOBILE_NO, MESSAGE,ID, 'N', ''  FROM [SMS_BILL_NOTIFICATION].[dbo].[daily_billrun_ODS] WHERE STATUS = 'N')" +

                                    " UPDATE [SMS_BILL_NOTIFICATION].[dbo].[daily_billrun_ODS] SET STATUS = 'C' WHERE STATUS = 'N' ";

                    var sqlQueryBatchDetai = PetaPoco.Sql.Builder.Append(sqlStrBatchDetail, system_id);

                    db.Execute(sqlStrBatchDetail);

                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("(InsertSMSOutGoing) Exception triggered", ex);
                return false;
            }
        }



        public bool UpdateDailyBillRunFinal()
        {
            try
            {
                using (var db = new Database("BillNotification"))
                {
                    var sqlStr = " UPDATE [SMS_BILL_NOTIFICATION].[dbo].[daily_billrun_ODS] SET STATUS = 'C' WHERE STATUS = 'N' ";

                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr);

                    db.Execute(sqlStr);

                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("(Update DAILYBILLRUN) Exception triggered", ex);
                return false;
            }
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            try
            {
                DataTable dataTable = new DataTable(typeof(T).Name);

                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
            catch (Exception ex)
            {
                log.Error("(ToDataTable) Exception triggered", ex);
                return null;
            }
        }


    }
}


