﻿using Ionic.Zip;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class ReadSource
    {
        private ILog log;

        public ReadSource()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(ReadSource));
        }

        public List<UnzipFiles> UnzipFiles(string fullFilePath)
        {
            try
            {
                var fileList = new List<UnzipFiles>();

               // var impersonationContext = new WrappedImpersonationContext("astro.com.my", "MBN$9500", "SMSGATEWAYU$er");
                //impersonationContext.Enter();

                using (ZipFile zip = ZipFile.Read(fullFilePath))
                {
                    //var selection = (from e in zip.Entries
                    //                 where (e.FileName).StartsWith("CSS/")
                    //                 select e);

                    //var path = Directory.CreateDirectory(outputPath);

                    foreach (var e in zip.Entries)
                    {
                        var strArray = e.FileName.Split('/');

                        // ignore sub directory
                        if (strArray.Count() == 2)
                        {
                            e.Extract(AppDomain.CurrentDomain.BaseDirectory + @"Unzipped Files\", ExtractExistingFileAction.Throw);

                            if (!e.IsDirectory)
                            {
                                var fileObj = new UnzipFiles();
                                fileObj.fileName = e.FileName;
                                fileObj.fileSize = e.UncompressedSize;
                                fileList.Add(fileObj);
                            }
                        }
                    }
                }

                //impersonationContext.Leave();

                return fileList;
            }
            catch (Exception ex)
            {
                log.Error(String.Format("(UnzipFiles) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }

        }

        public string SearchFile(List<UnzipFiles> files,DateTime yDay)
        {
            try
            {
                long sizeCounter = 0;
                string selectedFileName = string.Empty;

                foreach (var file in files)
                {
                    var splitStr = file.fileName.Split('.');
                    var output = 0;
                    var tryParse = Int32.TryParse(splitStr[1], out output);

                    if (tryParse)
                    {
                        var firstVal = yDay.ToString("00ddMMyyyy");
                        var secondVal = yDay.ToString("yyyyMMdd");

                        if (firstVal == splitStr[1] && secondVal == splitStr[2])
                        {

                            if (file.fileSize > sizeCounter)
                            {
                                sizeCounter = file.fileSize;
                                selectedFileName = file.fileName;
                            }

                        }

                        // for single file // remove, not using min value anymore

                        //if (yDay == DateTime.MinValue)
                        //{
                        //    if (file.fileSize > sizeCounter)
                        //    {
                        //        sizeCounter = file.fileSize;
                        //        selectedFileName = file.fileName;
                        //    }
                        //}
                    }
                }

                return selectedFileName;

            }
            catch (Exception ex)
            {
                log.Error(string.Format("(SearchFile) Exception triggered : \r\n {0}", ex.ToString()));
                return string.Empty;
            }
        }

        public List<string> ReadFile(string path, string fileName)
        {
            try
            {
                var filePath = path + fileName;
                var lineCounter = 0;

                var strList = new List<string>();

                foreach (var line in File.ReadLines(filePath))
                {
                    lineCounter++;

                    if (lineCounter < 5)
                    {
                        continue;
                    }

                    var strArray = line.Split('\t');

                    if (strArray[2] == "S")
                    {
                        strList.Add(strArray[0]);
                    }

                }

                return strList;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(ReadFile) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }

        }

        public List<string> ReadFileAdhoc(string path, string fileName)
        {
            try
            {
                var filePath = path + fileName;
                var lineCounter = 0;

                var strList = new List<string>();

                foreach (var line in File.ReadLines(filePath))
                {
                    lineCounter++;
                    strList.Add(line.Trim());                    
                }

                return strList;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(ReadFile) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }

        }
    }
}
