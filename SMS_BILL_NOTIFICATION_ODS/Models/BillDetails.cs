﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class BillDetails
    {
        public BillDetails()
        {
            mobileNo = "0123456789";
        }

        public string accountNo { get; set; }
        public DateTime statement_date { get; set; }
        public decimal newcharges { get; set; }
        public decimal overdueamt { get; set; }
        public decimal total_amt_due { get; set; }
        public DateTime due_date { get; set; }
        public decimal lastpaymentamount { get; set; }
        public DateTime lastpaymentdate { get; set; }
        public string mobileNo { get; set; }
        public string m { get; set; }
    }

    public class LastPayment
    {
        public decimal amount { get; set; }
        public DateTime activity_date { get; set; }
    }

    public class DailyBillRun
    {
        public string message { get; set; }
        public string bill_date { get; set; }
        public string mobile_no { get; set; }
        public string account_id { get; set; }
        public string status { get; set; }
        public string fbilldate { get; set; }
        public int BillRunSummaryId_FK { get; set; }
    }

    public class AccountNoLog
    {
        public int Id { get; set; }
        public string AccountNo { get; set; }
        public string SMSMessage { get; set; }
        public string bill_date { get; set; }
        public string mobile_no { get; set; }
        public string status { get; set; }
        public string fbilldate { get; set; }
        public decimal newcharges { get; set; }
        public decimal overdueamt { get; set; }
        public decimal total_amt_due { get; set; }
        public DateTime due_date { get; set; }
        public decimal lastpaymentamount { get; set; }
        public string lastpaymentdate { get; set; }
        public string Message { get; set; }
        public string Flag { get; set; }
        public string Origin { get; set; }
        public int BillRunSummaryId_FK { get; set; }
    }

    public class BillRunSummary
    {
        public int Id { get; set; }
        public string cycle { get; set; }
        public int totalCount { get; set; }
        public int sentCount { get; set; }
        public int excludedCount { get; set; }
        public int errorCount { get; set; }
        public int filteredCount { get; set; }
    }

    public class AccountNoTotalCRM
    {
        public string ACCOUNT_ID { get; set; }
        public int BillRunSummaryId_FK { get; set; }
    }
    public class BatchHeaderValue
    {
        public int ID { get; set; }
    }

    public class AccountDetails
    {
        public int ACCOUNT_NUMBER { get; set; }
    }
}