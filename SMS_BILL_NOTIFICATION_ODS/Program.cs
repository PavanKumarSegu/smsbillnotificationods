﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace SMS_BILL_NOTIFICATION_ODS
{
    class Program
    {
        private static ILog log;

        static void Main(string[] args)
        {
            try
            {   




                log4net.Config.XmlConfigurator.Configure();
                log = LogManager.GetLogger(typeof(Program));

                log.Info("Program started");
                //Program objProg = new Program();
                //objProg.SendMail();


                //var accountdailylist = new filemanager().getaccountnodaily();
                //if (!accountdailylist.isnullorempty())
                //{
                //    var insertstatus = new smsmanager().runtask(accountdailylist);
                //    if (!insertstatus)
                //    {
                //        log.error(string.format("(main) the is error committing / inserting daily records into database \r\n"));
                //        new sendemailalert().send("(main) the is error committing / inserting daily records into database \r\n please check error log \r\n timestamp : " + datetime.now.tostring());
                //    }
                //}
                //else
                //{
                //    log.error(string.format("(main) daily zip file not found / readable \r\n"));
                //    new sendemailalert().send("(main) daily zip file not found / readable : \r\n please check error log \r\n timestamp : " + datetime.now.tostring());
                //}

                //Commented by Pavan
                var accountList = new FileManager().GetAccountNoSingle();

                if (!accountList.IsNullOrEmpty())
                {
                    var insertDailyStatus = new SMSManager().RunTask(accountList);
                }
                else
                {
                    log.Error(string.Format("(Main) Zip file not found / readable : \r\n"));
                }
                //End


                //var accountList = new FileManager().ReadAccNoAdhoc("SMSBILLCATCHUP_2.txt");
                //if (!accountList.IsNullOrEmpty())
                //{
                //    var insertDailyStatus = new SMSManager().RunTask(accountList);
                //}


                //var list = new SQLQueries().SelectAccountRegistered();

                //var splitList = new List<List<acc>>();

                //for (int i = 0; i < list.Count; i += 1000)
                //{
                //    splitList.Add(list.GetRange(i, Math.Min(1000, list.Count - i)));
                //}

                //var counter = 0;
                //var masterList = new List<BillDetails>();
                //foreach (var minilist in splitList)
                //{
                //    var list2 = GetBillDetails(minilist);

                //    masterList.AddRange(list2);

                //    counter++;
                //    Debug.WriteLine("cycle : " + counter);
                //    Debug.WriteLine("count : " + list2.Count);

                //}


                //var str = CreateCSVTextFile(masterList); 


                log.Info("Program completed");
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(Main) Exception triggered : \r\n", ex.ToString()));
            }
            
        }
        private void SendMail()
        {
            try
            {
                string to = "pavan.segu@valuelabs.com";
                string from = "noreply@astro.com.my";
                MailMessage message = new MailMessage(from, to);
                message.To.Add(new MailAddress("vijayanand_rajan@astro.com.my"));
                message.Subject = "Test Email";
                message.Body = "Test";
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("EXCPRDHUB00.astro.net");
                client.Port = 25;
                client.Send(message);
                Console.WriteLine("Succcss");
                Console.ReadLine();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured"+ex.Message +" Inner stacktrace: "+ex.StackTrace);
                Console.ReadLine();
            }
        }


        //public static List<BillDetails> GetBillDetails(List<acc> AccountNoList)
        //{
        //    try
        //    {
        //        var strB = new StringBuilder();
        //        strB.Append("( ");
        //        foreach (var accountNo in AccountNoList)
        //        {
        //            strB.Append("'" + accountNo.ACCOUNT_ID + "',");
        //        }

        //        var accountNoStr = strB.ToString();
        //        accountNoStr = accountNoStr.Remove(accountNoStr.Length - 1) + " ) ";


        //        var billList = new SQLQueries().SelectBillDetails(accountNoStr);

        //        return billList;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(string.Format("(GetBillDetails) Exception triggered : \r\n {0}", ex.Message));
        //        return null;
        //    }
        //}

        //private static string CreateCSVTextFile<T>(List<T> data)
        //{
        //    var properties = typeof(T).GetProperties();
        //    var result = new StringBuilder();

        //    foreach (var row in data)
        //    {
        //        var values = properties.Select(p => p.GetValue(row, null))
        //                               .Select(v => StringToCSVCell(Convert.ToString(v)));
        //        var line = string.Join(",", values);
        //        result.AppendLine(line);
        //    }

        //    return result.ToString();
        //}

        //private static string StringToCSVCell(string str)
        //{
        //    bool mustQuote = (str.Contains(",") || str.Contains("\"") || str.Contains("\r") || str.Contains("\n"));
        //    if (mustQuote)
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        sb.Append("\"");
        //        foreach (char nextChar in str)
        //        {
        //            sb.Append(nextChar);
        //            if (nextChar == '"')
        //                sb.Append("\"");
        //        }
        //        sb.Append("\"");
        //        return sb.ToString();
        //    }

        //    return str;
        //}
    }
}


