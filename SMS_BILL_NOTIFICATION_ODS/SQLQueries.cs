﻿using log4net;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class SQLQueries
    {
        private ILog log;

        public SQLQueries()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(SQLQueries));
        }



        //public List<BillDetails> SelectBillDetails(string accountId)
        //{
        //    try
        //    {
        //        using (var db = new Database("LocalDB"))
        //        {
        //            var sqlStr = "select CAST(cast(accountNo as decimal) as varchar(20)) 'accountNo',CONVERT(datetime,statement_date,103) 'statement_date',newcharges ,'0.00' 'overdueamt', total_amt_due  'total_amt_due',CONVERT(datetime,due_date,103) 'due_date','0.00' 'lastpaymentamount',cast('' as datetime) 'lastpaymentdate' ,CONCAT('60',CAST(cast(mobileNo as decimal) as varchar(20))) 'mobileNo'  from ODSTempTable";
        //            //" AND not exists (select 1 from SMS_BILL_NOTIFICATION.[dbo].[DAILY_BILLRUN_FINAL] where dbo.check_digit(account_id) = dbo.check_digit(cte.accountNo) and bill_date = CONVERT(varchar,cte.statement_date,3))";
        //            log.Info(string.Format("Query : ", sqlStr));

        //            var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr);
        //            db.CommandTimeout = 4 * 60;
        //            var result = db.Fetch<BillDetails>(sqlQuery);
        //            return result;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(string.Format("(SelectBillDetails) Exception triggered : \r\n {0} ", ex.ToString()));
        //        return null;
        //    }

        //}


        public List<BillDetails> SelectBillDetails(string accountId)
        {
            try
            {
                using (var db = new Database("IMPODS01"))
                {
                    var sqlStr = ";WITH cte AS " +
                                    " ( " +
                                    "    SELECT b.ba_account_no as 'accountNo',  " +
                                    "    statement_date,  " +
                        //"    DAY(statement_date) as 'm', " +
                                    "    total_amt_due-(prev_balance_amt+ total_finance_act) as 'newcharges',  " +
                                    "    prev_balance_amt+ total_finance_act as 'overdueamt',  " +
                                    "    total_amt_due,  " +
                                    "    due_date,   " +
                                    "    d.amount as 'lastpaymentamount',  " +
                                    "    d.activity_date as 'lastpaymentdate' , " +
                                    "    isnull(MOBILE_PHONE,X_ALTERN_PHONE) AS 'mobileNo', " +
                                    "          ROW_NUMBER() OVER (PARTITION BY b.ba_account_no ORDER BY statement_date DESC " +
                                    "       ,activity_date DESC " +
                                    "      ) AS rn  " +
                                    "    FROM   " +
                                    " 	IMPODS01.dbo.V_BL1_BILL_STATEMENT  a  " +
                                    " 	INNER JOIN   " +
                                    " 		IMPODS01.dbo.V_bl1_blng_arrangement  b on a.ba_no=b.ba_no  " +
                                    " 	LEFT OUTER JOIN   " +
                                    " 		IMPODS01.dbo.V_Bl1_Bill_finance_act c on b.ba_no=c.ba and a.period_key=c.period_key  " +
                                    " 	LEFT OUTER JOIN   " +
                                    " 		IMPODS01.dbo.V_AR1_PAYMENT d on d.account_id = b.ba_account_no  " +
                                    " 	INNER JOIN   " +
                                    "       IMPODS01.dbo.V_TABLE_BUS_ORG e on e.ORG_ID = convert(varchar(20),b.ba_account_no) " +
                                    "   LEFT OUTER JOIN " +
                                    "       IMPODS01.dbo.V_TABLE_CONTACT_ROLE f on f.CONTACT_ROLE2BUS_ORG = e.OBJID " +
                                    "   LEFT OUTER JOIN " +
                                    "       IMPODS01.dbo.V_TABLE_CONTACT g on g.OBJID = f.CONTACT_ROLE2CONTACT " +
                                    " 	WHERE   " +
                                    " 		b.ba_account_no IN  " + accountId +
                        //" AND not exists (select 1 from SMS_BILL_NOTIFICATION.[dbo].[daily_billrun_final_ods] where acct_id = b.ba_account_no) " +
                                    " ) " +
                                    " SELECT *  " +
                                    " FROM cte " +
                                    " WHERE rn = 1 ";
                    //" AND not exists (select 1 from SMS_BILL_NOTIFICATION.[dbo].[DAILY_BILLRUN_FINAL] where dbo.check_digit(account_id) = dbo.check_digit(cte.accountNo) and bill_date = CONVERT(varchar,cte.statement_date,3))";
                    log.Info(string.Format("Query : ", sqlStr));

                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr);
                    db.CommandTimeout = 4 * 60;
                    var result = db.Fetch<BillDetails>(sqlQuery);
                    return result;
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(SelectBillDetails) Exception triggered : \r\n {0} ", ex.ToString()));
                return null;
            }

        }

        public List<BillDetails> SelectBlastedBillDetails(string accountId)
        {
            try
            {
                using (var db = new Database("BillNotification"))
                {
                    var sqlStr = ";WITH cte AS " +
                                    " ( " +
                                    "    SELECT b.ba_account_no as 'accountNo',  " +
                                    "    statement_date, " +
                                    "    d.activity_date as 'lastpaymentdate' , " +
                                    "          ROW_NUMBER() OVER (PARTITION BY b.ba_account_no ORDER BY statement_date DESC " +
                                    "       ,activity_date DESC " +
                                    "      ) AS rn  " +
                                    "    FROM   " +
                                    " 	IMPODS01.dbo.V_BL1_BILL_STATEMENT  a  " +
                                    " 	INNER JOIN   " +
                                    " 		IMPODS01.dbo.V_bl1_blng_arrangement  b on a.ba_no=b.ba_no  " +
                                    " 	LEFT OUTER JOIN   " +
                                    " 		IMPODS01.dbo.V_AR1_PAYMENT d on d.account_id = b.ba_account_no  " +
                                    " 	WHERE   " +
                                    " 		b.ba_account_no IN  " + accountId +
                        " AND exists (select 1 from SMS_BILL_NOTIFICATION.[dbo].[daily_billrun_final_ods] where account_id = b.ba_account_no and bill_date = CONVERT(varchar,statement_date,3))" +
                                    " ) " +
                                    " SELECT *  " +
                                    " FROM cte " +
                                    " WHERE rn = 1 ";
                                    //" AND exists (select 1 from SMS_BILL_NOTIFICATION.[dbo].[DAILY_BILLRUN_FINAL] where dbo.check_digit(account_id) = dbo.check_digit(cte.accountNo) and bill_date = CONVERT(varchar,cte.statement_date,3))";

                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr);
                    db.CommandTimeout = 4 * 60;
                    var result = db.Fetch<BillDetails>(sqlQuery);
                    return result;
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(SelectBillDetails) Exception triggered : \r\n {0} ", ex.ToString()));
                return null;
            }

        }

        public List<LastPayment> SelectLastPayment(string accountId)
        {
            try
            {
                using (var db = new Database("IMPODS01"))
                {
                    var sqlStr = "select top 1 " +
                                        "amount, " +
                                        "activity_date  " +
                                    "from  " +
                                        "[IMPODS01].[dbo].[V_AR1_PAYMENT] a " +
                                    "where  " +
                                        "a.account_id= @0 " +
                                    "order by a.activity_date desc ";
                    var sqlQuery = PetaPoco.Sql.Builder.Append(sqlStr, accountId);
                    var result = db.Fetch<LastPayment>(sqlQuery);

                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<AccountNoTotalCRM> SelectAccountRegistered()
        {
            try
            {
                using (var db = new Database("Prod"))
                {
                    var qStr = "SELECT [ACCOUNT_ID] FROM [SMS_BILL_NOTIFICATION].[dbo].[ACCOUNT_REGISTERED] with (nolock) where STATUS = 'c'";
                    var sqlQuery = PetaPoco.Sql.Builder.Append(qStr);
                    db.CommandTimeout = 4 * 60;
                    var result = db.Fetch<AccountNoTotalCRM>(sqlQuery);
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public int InsertBillRunSummary(BillRunSummary summary)
        {
            try
            {
                using (var db = new Database("BillNotification"))
                {
                    var Id = (int)db.Insert("BillRunSummary", "Id", summary);
                    return Id;
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("(InsertBillRunSummary) Exception triggered : \r\n {0} ", ex.ToString()));
                return 0;
            }
        }

    }


}


