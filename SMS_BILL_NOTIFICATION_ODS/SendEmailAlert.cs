﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class SendEmailAlert
    {
         private ILog log;

         public SendEmailAlert()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(ReadExcel));
        }

        public bool send(string msg)
        {
            try
            {
                string to = ConfigurationManager.AppSettings["MAILTO"].ToString().Trim();
                string cc = ConfigurationManager.AppSettings["MAILCC"].ToString().Trim();
                MailAddress fromAdd = new MailAddress("noreply@astro.com.my");
                MailMessage mail = new MailMessage();
                mail.To.Add(to);
                mail.CC.Add(cc);
                mail.From = fromAdd;
                mail.Body = msg;
                mail.Subject = "SMS Bill Notification Logs";
                mail.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("EXCPRDHUB00", 25);
                client.Send(mail);
                mail = null;
                client = null;

                return true;
            }
            catch (Exception ex)
            {
                log.Error(String.Format("(Send) Exception triggered : \r\n {0}", ex.ToString()));
                return false;
            }
        }
    }
}
