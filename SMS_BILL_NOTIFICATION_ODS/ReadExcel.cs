﻿using Excel;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace SMS_BILL_NOTIFICATION_ODS
{
    public class ReadExcel
    {
        private ILog log;

        public ReadExcel()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(ReadExcel));
        }
        public List<string> Read()
        {
            try
            {
                var filePath = AppDomain.CurrentDomain.BaseDirectory + @"SuppressionList\" + "Bill Suppress_7Oct_until_6Dec.xlsx";

                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                //...
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                //...
                //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();

                var dtable = result.Tables[0];
                //...
                //4. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                //DataSet result = excelReader.AsDataSet();

                var list = new List<string>();

                //5. Data Reader methods
                while (excelReader.Read())
                {
                    list.Add(excelReader.GetString(0));
                }

                return list;
            }
            catch (Exception ex)
            {
                log.Error(String.Format("(Read) Exception triggered : \r\n {0}", ex.ToString()));
                return null;
            }
        }
    }
}
